PARAMS = {
    'resistance': 3,
    'capacity': 2,
    'spike_value': 0.7,
    'voltage_rest': -0.65,
    'voltage_reset': -0.7,
    'firing_threshold': -0.05,
    'dt': 0.1,
    'total_time': 120,
    'delta_t': 0.3,
    'a': 3,
    'b': 0.5,
    'tau_w': 0.1,
    'current': 0,

}
TOTAL_TIME = PARAMS['total_time'] / PARAMS['dt']

NETWORK_PARAMS = {
    'excitatory_size': 800,
    'inhibitory_size': 200,
    'internal_weight': 0.001,
    'internal_connection_probability': 0.3,
    'alpha_rate': 1,
    'alpha_rate_populations': 0.9,
    'current_initial': 0.4
}

MODEL = {
    'lif': 'leaky integrated firing model',
    'nlif': 'exponential leaky integrated firing model',
    'anlif': 'adaptive exponential leaky integrate firing model'
}

START_CURRENT = 0
END_CURRENT = 3
INITIAL_CURRENT = 1

START_TIME = 10
END_TIME = 60
INITIAL_TIME = 20

START_CAPACITY = 0
END_CAPACITY = 10
INITIAL_CAPACITY = 3

START_RESISTANCE = 0
END_RESISTANCE = 10
INITIAL_RESISTANCE = 3

START_TAU_W = 0
END_TAU_W = 2
INITIAL_TAU_W = 0.8

INHIBITORY_FIRING_THRESHOLD = -0.3
