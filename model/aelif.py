from .lif import LIF
import numpy as np


class AELIF(LIF):
    def __init__(self, PARAMS):
        super().__init__(PARAMS)
        self.a = PARAMS['a']
        self.b = PARAMS['b']
        self.delta_t = PARAMS['delta_t']
        self.tau_w = PARAMS['tau_w']
        self.initialize_w()
        self.model = 'anlif'

    def initialize_w(self):
        self.w = np.zeros(len(self.time))

    def compute_voltage(self, i):
        self.initialize_w()
        self.initialize_voltage()
        self.spike_times.append(0)
        for i in range(1, len(self.time)):
            self.compute_w(i)
            dv_per_dt = (-(self.voltage[i - 1] - self.voltage_rest) + self.delta_t * np.exp((self.voltage[i - 1] - self.firing_threshold) / self.delta_t) - self.resistance * self.w[i] + self.resistance * self.current[i]) / (self.resistance * self.capacity)
            self.voltage[i] = self.voltage[i - 1] + dv_per_dt * self.dt
            if self.voltage[i] > self.firing_threshold:
                self.voltage[i - 1] = self.spike_value
                self.voltage[i] = self.voltage_reset
                self.spike_number += 1
                self.spike_times.append(i)

    def compute_w(self, i):
        dw_per_dt = (self.a * (self.voltage[i - 1] - self.voltage_rest) - self.w[i - 1] + self.b * self.tau_w * int(1 - np.sign(self.time[i] - self.spike_times[-1]))) / (self.tau_w)
        self.w[i] = self.w[i - 1] + self.dt * dw_per_dt
