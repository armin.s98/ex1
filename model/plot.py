import matplotlib.pyplot as plt
from matplotlib.pyplot import Button, Slider


def create_figure(name):
    fig = plt.figure(name, figsize=(14, 7))
    ax = fig.add_subplot(121, xlabel='time[s]', ylabel='potential[v]')
    plt.title(name)
    fig.subplots_adjust(bottom=0.3)
    return fig, ax


def add_plot(fig):
    ax = fig.add_subplot(122, xlabel='current', ylabel='frequency')
    plt.title("frequency")
    return ax


def add_line(name, par1, par2):
    line = plt.plot(par1, par2, label=name)[0]
    return line


def add_slider(position, axis_color, name, start, end, initial_value):
    slider_axis = plt.axes(position, facecolor=axis_color)
    slider = Slider(slider_axis, name, start, end, valinit=initial_value)
    return slider


def add_legend(location):
    plt.legend(loc=location)


def show():
    plt.show()
