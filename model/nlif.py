from .lif import LIF
import numpy as np


class NLIF(LIF):
    def __init__(self, PARAMS):
        super().__init__(PARAMS)
        self.delta_t = PARAMS['delta_t']
        self.model = 'nlif'

    def compute_voltage(self, i):
        for i in range(1, len(self.time)):
            dv_per_dt = (-(self.voltage[i - 1] - self.voltage_rest) + self.delta_t * np.exp((self.voltage[i - 1] - self.firing_threshold) / self.delta_t) + self.resistance * self.current[i]) / (self.resistance * self.capacity)
            self.voltage[i] = self.voltage[i - 1] + dv_per_dt * self.dt
            if self.voltage[i] > self.firing_threshold:
                self.voltage[i - 1] = self.spike_value
                self.voltage[i] = self.voltage_reset
                self.spike_number += 1
                self.spike_times.append(i)
        return self.voltage
