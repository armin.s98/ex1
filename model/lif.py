import matplotlib.pyplot as plt
import random as rnd
import numpy as np
from random import randint
from scipy import interpolate
from sklearn.preprocessing import MinMaxScaler
from .plot import *
# from .settings import settings import LIF_PARAMS_DEFAULT
from .constant import *


class LIF:
    voltage = None
    current = None
    time = None

    def __init__(self, data):
        self.resistance = data['resistance']
        self.capacity = data['capacity']
        self.spike_value = data['spike_value']
        self.voltage_rest = data['voltage_rest']
        self.voltage_reset = data['voltage_reset']
        self.firing_threshold = data['firing_threshold']
        self.dt = data['dt']
        self.total_time = data['total_time']
        self.initialize_time()
        self.initialize_current()
        self.initialize_voltage()
        self.spike_times = np.zeros(len(self.time))
        self.spike_number = 0
        self.freq = list()
        self.current_list = list()
        self.type = 'excitatory'
        self.model = 'lif'

    def initialize_voltage(self):
        self.voltage = np.empty(len(self.time))
        self.voltage[0] = self.voltage_rest

    def initialize_current(self):
        self.current = np.zeros(len(self.time))

    def set_current(self, start=0, end=250000, value=0, random=False):
        if not random:
            self.current[start:end] = value
        else:
            x = np.linspace(0, 1, end - start)
            y = randint(0, 100) * np.sin(randint(0, 100) * x) + randint(0, 100) * np.cos(randint(0, 100) * x)
            y = MinMaxScaler(feature_range=(-1, 1)).fit_transform(y.reshape(-1, 1)).reshape(-1)
            y = [value + abs(x) for x in y]
            # y = [rnd.uniform(0.5, 1) for x in y]
            self.current[start:end] = y

    def initialize_time(self):
        self.time = np.arange(0, self.total_time, self.dt)

    def compute_voltage(self, i):
        dv_per_dt = (-(self.voltage[i-1] - self.voltage_rest) + self.resistance * self.current[i]) / (self.resistance * self.capacity)
        self.voltage[i] = self.voltage[i-1] + dv_per_dt * self.dt
        if self.voltage[i] > self.firing_threshold:
            self.voltage[i] = self.voltage_reset
            self.spike_number += 1
            self.spike_times[i] = 1

    def compute_frequency(self):
        self.current_list = np.arange(0, 10, 0.5)
        for i in self.current_list:
            self.set_current(0, 60000, i)
            for j in range(1, 60000):
                self.compute_voltage(j)
            self.freq.append(self.spike_number / 60)

    def draw(self):
        fig, ax = create_figure(MODEL[self.model])
        voltage_line = add_line("voltage", self.time, self.voltage)
        current_line = add_line("current", self.time, self.current)
        ax1 = add_plot(fig)
        # ax1.ylabel('armin')
        frequency_line = add_line("frequency", self.current_list, self.freq)
        add_legend("upper right")
        ax.margins(2, 2)
        current_slider = add_slider([0.1, 0.15, 0.65, 0.03], 'yellow', 'current', START_CURRENT, END_CURRENT, INITIAL_CURRENT)

        def update_current(val):
            self.set_current(value=current_slider.val)
            current_line.set_ydata(self.current)
            for j in range(1, TOTAL_TIME):
                self.compute_voltage(j)
            voltage_line.set_ydata(self.voltage)
        current_slider.on_changed(update_current)

        total_time_slider = add_slider([0.1, 0.03, 0.65, 0.03], 'yellow', 'total_time', START_TIME, END_TIME, INITIAL_TIME)

        def update_total_time(val):
            ax.set_xlim(0, total_time_slider.val)
        total_time_slider.on_changed(update_total_time)

        capacity_slider = add_slider([0.1, 0.07, 0.65, 0.03], 'yellow', 'capacity', START_CAPACITY, END_CAPACITY, INITIAL_CAPACITY)

        def update_capacity(val):
            self.capacity = capacity_slider.val
            for i in range(1, TOTAL_TIME):
                self.compute_voltage(i)
            voltage_line.set_ydata(self.voltage)
        capacity_slider.on_changed(update_capacity)

        resistance_slider = add_slider([0.1, 0.11, 0.65, 0.03], 'yellow', 'resistance', START_RESISTANCE, END_RESISTANCE, INITIAL_RESISTANCE)

        def update_resistance(val):
            self.resistance = resistance_slider.val
            for i in range(1, TOTAL_TIME):
                self.compute_voltage(i)
            voltage_line.set_ydata(self.voltage)
        resistance_slider.on_changed(update_resistance)

        if self.model == 'anlif':
            tau_w_slider = add_slider([0.1, 0.19, 0.65, 0.03], 'yellow', 'tau w', START_TAU_W, END_TAU_W, INITIAL_TAU_W)

            def update_tau_w(val):
                self.tau_w = tau_w_slider.val
                for i in range(1, TOTAL_TIME):
                    self.compute_voltage(i)
                voltage_line.set_ydata(self.voltage)
            tau_w_slider.on_changed(update_tau_w)

        show()

