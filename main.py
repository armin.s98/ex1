from model.lif import LIF
from model.nlif import NLIF
from model.aelif import AELIF
from model.constant import *
from network.network import Network
import numpy as np
from model.constant import NETWORK_PARAMS


def lif_model():
    lif = LIF(PARAMS)
    lif.compute_frequency()
    lif.set_current(0, 250000, 1, False)
    lif.set_current(0, TOTAL_TIME, 0, True)
    for i in range(1, len(lif.time)):
        lif.compute_voltage(i)
    lif.draw()


def nlif_model():
    nlif = NLIF(PARAMS)
    nlif.compute_frequency()
    nlif.set_current(0, 250000, 1, False)
    nlif.set_current(300000, 600000, 0, True)
    nlif.compute_voltage()
    nlif.draw()


def anlif_model():
    aelif = AELIF(PARAMS)
    aelif.compute_frequency()
    aelif.set_current(0, 250000, 1, False)
    aelif.set_current(300000, 600000, 0, True)
    aelif.compute_voltage()
    aelif.draw()


def get_population_params(exc_size, inh_size, in_w, in_conn_p, alpha_r, current_initial):
    params = {
        'excitatory_size': exc_size,
        'inhibitory_size': inh_size,
        'internal_weight': in_w,
        'internal_connection_probability': in_conn_p,
        'alpha_rate': alpha_r,
        'current_initial': current_initial
    }
    return params


def one_population():
    network = Network()
    params = get_population_params(800, 200, 0.001, 0.4, 1, 0)
    network.add_population(params)
    population = network.populations[0]
    population.compute_activity()
    network.draw_raster_plot_population()


def three_population():
    network = Network()
    params = get_population_params(200, 0, 0.04, None, 1, 0)
    network.add_population(params)
    params = get_population_params(200, 0, 0.04, None, 1, 0.4)
    network.add_population(params)
    network.compute_network_activity()
    # params = get_population_params(0, 50, 0.001, 0.1, 1)
    # network.add_population(params)
    network.draw_network_plot()


instr = {
    'lif': lif_model,
    'nlif': nlif_model,
    'anlif': anlif_model,
    'one_population': one_population,
    'th': three_population
}
model = input("type your model {}".format(instr.keys()))
try:
    instr[model]()
except Exception as err:
    print(err)

