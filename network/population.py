import numpy as np
from model.lif import LIF
from model.constant import PARAMS
import matplotlib.pyplot as plt
from model.constant import NETWORK_PARAMS, TOTAL_TIME, INHIBITORY_FIRING_THRESHOLD
import random as rnd


class Population:
    def __init__(self, data):
        self.excitatory_size = data['excitatory_size']
        self.inhibitory_size = data['inhibitory_size']
        self.weight = data['internal_weight']
        self.current_initial = data['current_initial']
        self.neurons = dict()
        self.size = self.excitatory_size + self.inhibitory_size
        self.adjacency_matrix = np.zeros((self.size, self.size))
        self.st = int(TOTAL_TIME)
        self.current_plot_x = np.arange(0, self.st)
        self.current = np.zeros(self.st)
        self.spike_times_excitatory = list()
        self.spike_times_inhibitory = list()
        self.spike_number = 0
        self.initialize_neurons()

    def initialize_neurons(self):
        neuron_1 = LIF(PARAMS)
        neuron_1.set_current(0, self.st, self.current_initial, True)
        self.current = neuron_1.current.copy()
        self.neurons[0] = neuron_1
        for index in range(1, self.size):
            neuron = LIF(PARAMS)
            if index > self.excitatory_size:
                neuron.type = 'inhibitory'
                neuron.firing_threshold = INHIBITORY_FIRING_THRESHOLD
            neuron.current = neuron_1.current.copy()
            # neuron.set_current(0, self.st, self.current_initial, True)
            self.neurons[index] = neuron

    def compute_activity(self, t, extra_current):
        spike_time_excitatory = list()
        spike_time_inhibitory = list()
        self.spike_number = 0
        for index in range(self.size):
            neuron = self.neurons[index]
            if neuron.spike_times[t - 1] == 1:
                self.spike_number += 1
                if index < self.excitatory_size:
                    spike_time_excitatory.append(index)
                else:
                    spike_time_inhibitory.append(index)
                cnt = 0
                for i in range(self.size):
                    if self.adjacency_matrix[index][i] != 0 and i != index:
                        # print(self.adjacency_matrix[index][i])
                        z = 1
                        if index > self.excitatory_size:
                            z = -1
                        cnt += 1
                        self.neurons[i].current[t] += z * self.weight * NETWORK_PARAMS['alpha_rate'] + extra_current
        self.spike_times_excitatory.append(spike_time_excitatory)
        self.spike_times_inhibitory.append(spike_time_inhibitory)
        for index in range(self.size):
            neuron = self.neurons[index]
            neuron.compute_voltage(t)

    def draw_raster_plot(self):
        fig, (ax1, ax2) = plt.subplots(2, 1)
        ax1.eventplot(self.spike_times_excitatory, colors='blue', orientation='vertical')
        ax1.eventplot(self.spike_times_inhibitory, colors='red', orientation='vertical')
        ax1.set_title(label="raster plot")
        ax2.set_title(label="current")
        ax2.plot(self.current_plot_x, self.current)
        plt.show()

    def connection(self, random=False, probability=None):
        if random:
            print(probability)
            if probability is None:
                raise ValueError
            random_size = int(probability * float(self.size * self.size))
            size = np.product((self.size, self.size))
            board = np.zeros(size, dtype=np.int)
            j = np.random.choice(np.arange(size), random_size)
            board[j] = 1
            self.adjacency_matrix = board.reshape((self.size, self.size))
        else:
            print("full")
            self.adjacency_matrix = np.ones((self.size, self.size))
