import numpy as np
from model.lif import LIF
from network.population import Population
from model.constant import NETWORK_PARAMS, TOTAL_TIME, PARAMS
import matplotlib.pyplot as plt


class Network(object):
    def __init__(self):
        self.populations = list()
        self.st = TOTAL_TIME
        self.population1_activities = list()
        self.population2_activities = list()

    def add_population(self, params):
        population = Population(params)
        internal_connection_probability = NETWORK_PARAMS['internal_connection_probability']
        print(internal_connection_probability)
        if internal_connection_probability is not None:
            print("random")
            population.connection(True, internal_connection_probability)
        else:
            print("complete")
            population.connection(False, internal_connection_probability)
        self.populations.append(population)

    def compute_network_activity(self):
        activities = np.zeros(3)
        print(self.st)
        for t in range(1, int(self.st)):
            for i, population in enumerate(self.populations):
                if i == 0:
                    population.compute_activity(t, activities[1] * NETWORK_PARAMS['alpha_rate_populations'] * -1)
                else:
                    population.compute_activity(t, activities[0] * NETWORK_PARAMS['alpha_rate_populations'] * -1)
                activities[i] = population.spike_number
            self.population1_activities.append(activities[0] / self.populations[0].size)
            self.population2_activities.append(activities[1] / self.populations[1].size)

    def draw_raster_plot_population(self):
        for population in self.populations:
            population.draw_raster_plot()

    def draw_network_plot(self):
        fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
        ax1.set_ylabel("neurons")
        ax1.eventplot(self.populations[0].spike_times_excitatory, colors='blue', orientation='vertical')
        ax1.eventplot(self.populations[1].spike_times_excitatory, colors='orange', orientation='vertical')
        ax2.set_ylabel("activity")
        ax2.plot(np.arange(1, len(self.population1_activities) + 1, 1), self.population1_activities)
        ax2.plot(np.arange(1, len(self.population2_activities) + 1, 1), self.population2_activities)
        ax3.set_ylabel("stimulus")
        ax3.set_xlabel("Time")
        ax3.plot(self.populations[0].current_plot_x, self.populations[0].current)
        ax3.plot(self.populations[1].current_plot_x, self.populations[1].current)
        # ax3.plot(self.populations[0].current_plot_x, self.populations[0].neurons[0].current)
        plt.show()
